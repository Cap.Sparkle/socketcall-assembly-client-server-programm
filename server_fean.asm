[64 BITS]


section .bss

	addr: resq 2  
	descriptor: resq 1
	child_descriptor: resq 1 
	
	input_addr: resq 2
	input_len: resq 1
	
	msg_from_client: resb 256


section .text


_start:
	
	;SOCKET________________
		mov rax, 41 
        mov rdi, 2
        mov rsi, 1
        mov rdx, 0
        syscall 
        
        mov [descriptor], rax
        
        ;set the addres ===============
        mov [addr], dword 0x0a0a0200
        mov [addr + 4], dword 0x01cca8c0
        mov [addr + 8], dword 0x00
        mov [addr + 12], dword 0x00
        ;==============================
	
	;BIND________________
        mov rax, 49
		mov rdi, [descriptor]
		mov rsi, addr
		mov rdx, 16
		
	;LISTEN________________
		mov rax, 50
		mov rdi, [descriptor]
		mov rsi, 8

		
.accept:
		
		;is there no overflow vulnerabilities? =========== 
	;ACCEPT________________
		mov rax, 43
		mov rdi, [descriptor]
		mov rsi, input_addr
		mov rdx, input_len

		cmp rax, -1
		je .accept
		
		mov [child_descriptor], rax
		
	;FORK________________
		mov rax, 57
		syscall
		
		cmp rax, 0
		je .child
		jl .error
		
		jmp .accept
		
		;end of communication =========== 
		
		mov eax, 60
        syscall
	
		
		
.child:
		
	;READ________________	
		mov rax, 0
		mov rdi, [child_descriptor]
		mov rsi, msg_from_client
		mov rdx, 256
		syscall
			
	;WRITE________________
		mov rdx, rax
		
		mov rax, 1
		mov rdi, [new_s_desc]
		mov rsi, msg_from_client

		mov rax, [msg_from_client]
		
		cmp rax, 0x30
		jne .child
		


.error:
	
		; there is need to print error code
	
		
.end:
        mov eax, 60
        syscall
