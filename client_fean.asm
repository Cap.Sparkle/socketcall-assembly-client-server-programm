[BITS 64]

section .data

	msg db 'Feskovich_A', 0xa
	len dq 0xC

	
section .bss

	remaddr: resq 2  
	descriptor: resq 2

	
global _start

section .text
_start:

        ;socket
		mov rax, 41 
        mov rdi, 2
        mov rsi, 1
        mov rdx, 0
        syscall
       
        mov [descriptor], rax ;descriptor saving
        
        
        
        mov [remaddr], dword 0x0a0a0200
        mov [remaddr + 4], dword 0x01cca8c0
        mov [remaddr + 8], dword 0x00
        mov [remaddr + 12], dword 0x00
     
        
        ;connect
        mov rax, 42
        mov rdi, [descriptor]
        mov rsi, remaddr
        mov rdx, 16
        syscall
        
        
        ;send
        mov rax, 44
        mov rdi, [descriptor]
        mov rsi, [msg]
        mov rdx, [len]
        mov r10, 0
        mov r8, 0
.end:
        mov eax, 60
        syscall
        
